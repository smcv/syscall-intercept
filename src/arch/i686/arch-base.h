#ifndef _ARCH_SPECIFIC_H
#define _ARCH_SPECIFIC_H

#include <stdint.h>

/*
 * Kernel can clobber rcx and r11 while serving a syscall, those are ignored
 * The layout of this struct depends on the way the assembly wrapper saves
 * register on the stack.
 * Note: don't expect the SIMD array to be aligned for efficient use with
 * AVX instructions.
 */
struct sys_ctx {
	uint32_t eax;
	uint32_t ecx;
	uint32_t edx;
	uint32_t ebx;
	uint32_t esp;
	uint32_t ebp;

	uint32_t esi;
	uint32_t edi;
};

#endif
