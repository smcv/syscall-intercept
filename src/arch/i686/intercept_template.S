.globl intercept_asm_wrapper_tmpl
.globl intercept_asm_wrapper_tmpl_end
.globl intercept_asm_wrapper_wrapper_level1_addr
.globl intercept_asm_wrapper_patch_desc_addr

intercept_asm_wrapper_tmpl:
	mov $4, %eax
	mov $2, %ebx
	mov $hello_world, %ecx
	mov $25, %edx
	int $0x80
infinite:
	jmp infinite
intercept_asm_wrapper_patch_desc_addr:
intercept_asm_wrapper_wrapper_level1_addr:
	.word 1
intercept_asm_wrapper_tmpl_end:

.data
hello_world:
.string "entered wrapper looping\n"
