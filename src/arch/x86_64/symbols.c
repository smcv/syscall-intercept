#include <intercept.h>

/*
 * sym_get_rel_addr - Find address on a relocation entry.
 *
 * The constant SHT_RELA refers to "Relocation entries with addends" -- see the
 * elf.h header file.
 *
 */
unsigned char *
sym_get_rel_addr(struct intercept_desc *desc, const void *sym)
{
	const Elf64_Rela *rela = (Elf64_Rela *) sym;

	switch (ELF64_R_TYPE(rela->r_info)) {
	case R_X86_64_RELATIVE:
	case R_X86_64_RELATIVE64:
		/* Relocation type: "Adjust by program base" */

		debug_dump("jump target: %lx\n",
				(unsigned long)rela->r_addend);

		return desc->base_addr + rela->r_addend;
	}
	return NULL;
}
